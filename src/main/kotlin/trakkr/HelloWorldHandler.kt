package trakkr

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.util.*


@RestController
class HelloWorldHandler {


    @GetMapping("/api/hello")
    fun hello(): String = "Hello World"

    @GetMapping("api/now")
    fun now(): String {
        return LocalDate.now().toString()
    }
}