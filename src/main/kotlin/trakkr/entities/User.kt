package trakkr.entities

import javax.persistence.*

//Entity -> Representation of data in our table
// File names -> PascalCase

@Entity(name = "User")
@Table(name = "users")
data class User (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,


    @Column(
        nullable = false,
        updatable = true,
        name = "firstName"
    )
    var firstName: String,


    @Column(
        nullable = false,
        updatable = true,
        name = "lastName"
    )
    var lastName: String,


    @Column(
        nullable = false,
        updatable = false,
        name = "email"
    )
    var email: String,


    @Column(
        nullable = false,
        updatable = true,
        name = "isActive"
    )
    var isActive: Boolean? = true,


    @Column(
        nullable = false,
        updatable = true,
        name = "userType"
    )
    var userType: String
        ) {

}


/**
 * 1. Create Entity
 * 2. Create SQL
 * 3. Create Repository
 * 4. Create Service
 * 5. Implement Service
 * 6. Create Handler
 * Moving forward, repeat steps: 4-6...
 */