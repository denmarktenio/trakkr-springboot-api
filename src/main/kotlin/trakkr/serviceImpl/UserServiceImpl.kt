package trakkr.serviceImpl

import org.apache.coyote.Response
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import trakkr.entities.User
import trakkr.repositories.UserRepository
import trakkr.service.UserService


@Service
class UserServiceImpl(
    private val repository: UserRepository
) : UserService {
    /**
     * CREATE A USER
     */
    override fun createUser(user: User): User {
        // save the user using the UserRepository
        return repository.save(user)
    }

    /**
     * Get a specific user using an id
     */
    override fun getById(id: Long): User {
        val user = repository.findById(id).orElseThrow{
            ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "User with id: $id does not exist."
            )
        }
        return user
    }

    override fun updateUser(body: User, id: Long): User {
        // 1. Find the entity first.
        val user = repository.findById(id).orElseThrow{
            ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "User with id: $id is not found."
            )
        }
        // todo(Ensure data integrity)
        // 2. User save(). Creates if non-existent
        // updates if existent.
        return repository.save(user.copy(
            firstName = body.firstName,
            lastName = body.lastName,
            email = body.email
        ))
    }

    override fun deleteUser(id: Long) {
        val user = repository.findById(id).orElseThrow{
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist.")
        }
        repository.delete(user)
    }

    override fun getAllUsers(): List<User> {
        return repository.findAll().toList()
    }
}